<?php
include("inc.orgs.php");
include("inc.users.php");
include("inc.login.php");
include("inc.tools.php");
include('./../../db/db.php');

global $mytime;
$mytime=date("Y-m-d H:i:s",strtotime(date('Y-m-d H:i:s'))+(3600*8));
global $myip;
$myip=$_SERVER['SERVER_ADDR'];
global $pagepath;
$pagepath="http://$myip/qview/";
?>

<html>
<head>

    <?php
    if(isset($_REQUEST['menu']))
    {
        $menu=$_REQUEST['menu'];
        if($menu=='contributeform')
        {

        }
        elseif($menu!=='contributeprocess')
        {
            echo "<meta http-equiv='refresh' content='10; url=$pagepath' >";
        }
        else
        {
            echo "<meta http-equiv='refresh' content='5'>";
        }

    }
    ?>

    <!-- BOOTSTRAP -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
<div class="container">




<?php
    $orgid="";
    if(isset($_REQUEST['menu']))
    {
        //echo "test";
        $menu=$_REQUEST['menu'];
        if($menu=="vieworg")
        {
            $orgid=$_REQUEST['orgid'];
            echo vieworg($orgid);
        }
        elseif($menu=="contributeform")
        {
            $qid=$_REQUEST['qid'];
            echo contributeform($qid);
        }
        elseif($menu=="contributeprocess")
        {

            echo contributeprocess();
        }


    }
    else
        {
            ?>


    <div class="row">
        <!--   Page Title -->
        <div class="col-sm-12">
            <h1>Queue Vision</h1>
        </div>

        <!--   Places of Interest -->
        <?php echo listorgsmain();?>


    </div>
    <?php
        }

    ?>
</div>



</body>


</html>

